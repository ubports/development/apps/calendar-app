/*
 * Copyright (C) 2013-2016 Canonical Ltd
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtOrganizer 5.0
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import Lomiri.SyncMonitor 0.1
import Lomiri.Components.Popups 1.3
import SSO.OnlineAccounts 0.1


Page {
    id: calendarChoicePage
    objectName: "calendarchoicepopup"

    property var model
    signal collectionUpdated()

    function accountFromId(accountId)
    {
        if (accountId && accountId >= 0) {
            return Manager.loadAccount(accountId)
        }

        return null
    }

    visible: false
    header: PageHeader {
        id: header
        title: i18n.tr("Calendars")
        leadingActionBar.actions: Action {
            text: i18n.tr("Back")
            iconName: "back"
            onTriggered: {
                calendarChoicePage.collectionUpdated();
                pop();
            }
        }
        trailingActionBar.actions: Action {
            objectName: "syncbutton"
            iconName: "reload"
            // TRANSLATORS: Please translate this string  to 15 characters only.
            // Currently ,there is no way we can increase width of action menu currently.
            text: enabled ? i18n.tr("Sync") : i18n.tr("Syncing")
            onTriggered: syncMonitor.sync(["calendar"])
            enabled: (syncMonitor.state !== "syncing")
            visible: !networkError
        }
        flickable: flickable
    }

    SyncMonitor {
        id: syncMonitor
    }

    Flickable {
        id: flickable

        flickableDirection: Flickable.VerticalFlick
        anchors.fill: parent

        clip: true

        contentHeight: contents.height
        contentWidth: parent.width

        Column {
            id: contents
            spacing: units.gu(1)
            anchors {
                left: parent.left
                right: parent.right
            }

            ListItem {
                id: importFromGoogleButton

                visible: (onlineAccountHelper.status === Loader.Ready)
                anchors {
                    left: parent.left
                    right: parent.right
                }

                ListItemLayout {
                    id: onlineCalendarLayout
                    title.text: i18n.tr("Add online Calendar")

                    Image {
                        SlotsLayout.position: SlotsLayout.First
                        source: "image://theme/account"
                        width: units.gu(5)
                        height: width
                    }
                }

                onClicked: {
                    if (!networkError) {
                        onlineAccountHelper.item.run()
                    } else {
                        PopupUtils.open(offlineErrorComponent);
                    }
                }
            }

            Item {
                anchors {
                    left: parent.left
                    right: parent.right
                }

                height: calendarsList.height

                ListView {
                    id: calendarsList

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: childrenRect.height

                    model : calendarChoicePage.model.getCollections()
                    interactive: false
                    currentIndex: -1

                    delegate: ListItem {
                        id: delegateComp
                        objectName: "calendarItem"

                        height: calendarsListLayout.height + divider.height

                        ListItemLayout {
                            id: calendarsListLayout

                            Account {
                                id: delegateAccount
                                objectHandle: calendarChoicePage.accountFromId(modelData.extendedMetaData("collection-account-id"))
                            }

                            title.text: modelData.name
                            subtitle.text: delegateAccount.objectHandle ? delegateAccount.displayName : ""
                            title.objectName: "calendarName"

                            CheckBox {
                                id: checkBox
                                objectName: "checkBox"
                                SlotsLayout.position: SlotsLayout.Last
                                checked: modelData.extendedMetaData("collection-selected")
                                enabled: !calendarChoicePage.isInEditMode
                                onCheckedChanged: {
                                    if (!checkBox.checked && modelData.extendedMetaData("collection-readonly") === false) {
                                        var collections = calendarChoicePage.model.getWritableAndSelectedCollections();
                                        if (collections.length == 1) {
                                            PopupUtils.open(singleWritableDialogComponent);
                                            checkBox.checked = true;
                                            return;
                                        }
                                    }

                                    if (modelData.extendedMetaData("collection-selected") !== checkBox.checked) {
                                        modelData.setExtendedMetaData("collection-selected",checkBox.checked)
                                        var collection = calendarChoicePage.model.collection(modelData.collectionId);
                                        calendarChoicePage.model.saveCollection(collection);
                                    }
                                }
                            }

                            Rectangle {
                                id: calendarColorCode
                                objectName: "calendarColorCode"

                                SlotsLayout.position: SlotsLayout.First
                                width: units.gu(5)
                                height: width
                                color: modelData.color
                                opacity: checkBox.checked ? 1.0 : 0.8

                                MouseArea{
                                    anchors.fill: parent
                                    onClicked: {
                                        //popup dialog
                                        var dialog = PopupUtils.open(Qt.resolvedUrl("ColorPickerDialog.qml"),calendarChoicePage);
                                        dialog.accepted.connect(function(color) {
                                            var collection = calendarChoicePage.model.collection(modelData.collectionId);
                                            collection.color = color;
                                            calendarChoicePage.model.saveCollection(collection);
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
            }

            SimpleDivider { }

            ListItem {
                ListItemLayout {
                    title.text: i18n.tr("Import events")
                    ProgressionSlot {}
                }
                onClicked: {
                    pageStack.push(importComp)
                }
            }

            ListItem {
                divider.visible: false
                height: exportLblLayout.height + divider.height
                ListItemLayout {
                    id: exportLblLayout
                    title.text: i18n.tr("Export calendar")
                    ProgressionSlot {}
                }
                onClicked: {
                    PopupUtils.open(exportDialog, calendarChoicePage)
                }

            }
        }
    }

    Component {
        id: importComp
        Page {
            id: importPicker
            visible: false
            header: PageHeader {
                id: importPickerHeader
                title: i18n.tr("Choose from")
            }

            property var activeTransfer

            ContentPeerPicker {
                anchors.top: importPickerHeader.bottom
                anchors.topMargin: units.gu(1)
                handler: ContentHandler.Source
                contentType: ContentType.Events
                showTitle: false

                onPeerSelected: {
                    peer.selectionType = ContentTransfer.Single;
                    importPicker.activeTransfer = peer.request();
                    pageStack.pop();
                }

                onCancelPressed: pageStack.pop();
            }

            ContentTransferHint {
                anchors.fill: parent
                activeTransfer: importPicker.activeTransfer
            }
        }
    }


    Component {
        id: exportDialog

        Dialog {
            id: dialogue

            title: i18n.tr("Calendar export")

            Column {

                anchors.left: parent.left
                anchors.right: parent.right
                spacing: units.gu(2)

                OptionSelector{
                    id: calendarsOption
                    objectName: "calendarsOption"
                    enabled: !checkBox.checked

                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: i18n.tr("Select your calendar")                  

                    function init() {
                        calendarsOption.model = calendarChoicePage.model ? calendarChoicePage.model.getCollections() : []
                        const collectionId = calendarChoicePage.model.getDefaultCollection().collectionId
                        var index = 0;
                        for(var i=0; i < calendarsOption.model.length; ++i){
                            if(calendarsOption.model[i].collectionId === collectionId){
                                index = i;
                                break;
                            }
                        }
                        calendarsOption.selectedIndex = index
                    }

                    Connections {
                        target: calendarChoicePage.model ? calendarChoicePage.model : null
                        onModelChanged: calendarsOption.init()
                        onCollectionsChanged: calendarsOption.init()
                    }

                    Connections {
                        target: calendarChoicePage
                        onActiveChanged: {
                            if (calendarChoicePage.active) {
                                calendarsOption.init()
                            }
                        }
                    }

                    delegate: OptionSelectorDelegate{
                        text: modelData.name
                    }
                    onExpandedChanged: Qt.inputMethod.hide();

                    Component.onCompleted: init()
                }

                ListItem {
                    height: layout.height + divider.height
                    divider { visible: false; }
                    ListItemLayout {
                        id: layout
                        title.text: i18n.tr("Select all")
                        CheckBox {
                            id: checkBox
                            SlotsLayout.position: SlotsLayout.Last
                        }
                    }

                    onClicked: {
                        checkBox.checked = !checkBox.checked;
                    }
                }

                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(4)

                    Button {
                        text: i18n.tr("Cancel")
                        onClicked:  {
                            PopupUtils.close(dialogue)
                        }
                    }

                    Button {
                        text: i18n.tr("Proceed")
                        color: theme.palette.normal.positive
                        onClicked:  {
                            var collectionIds = []
                            if (checkBox.checked) {
                                collectionIds = calendarsOption.model.map( collection => collection.collectionId)
                            } else {
                                collectionIds.push(calendarsOption.model[calendarsOption.selectedIndex].collectionId)
                            }

                            console.log('selected collectionIds for export:', collectionIds)
                            pageStack.push(Qt.resolvedUrl("ExportCalendarDialog.qml"), {"collectionIds": collectionIds});
                            PopupUtils.close(dialogue)
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: onlineAccountHelper

        // if running on test mode does not load online account modules
        property string sourceFile: Qt.resolvedUrl("OnlineAccountsHelper.qml")

        anchors.fill: parent
        asynchronous: true
        source: sourceFile
    }

    Component {
        id: singleWritableDialogComponent
        Dialog {
            id: singleWritableDialog
            title: i18n.tr("Unable to deselect")
            text: i18n.tr("In order to create new events you must have at least one writable calendar selected")
            Button {
                text: i18n.tr("Ok")
                onClicked: PopupUtils.close(singleWritableDialog)
            }
        }
    }

    Component {
        id: offlineErrorComponent
        Dialog {
            id: offlineErrorDialog
            title: i18n.tr("Network required")
            text: i18n.tr("You are currently offline. In order to add online accounts you must have network connection available.")
            Button {
                text: i18n.tr("Ok")
                onClicked: PopupUtils.close(offlineErrorDialog)
            }
        }
    }
}
