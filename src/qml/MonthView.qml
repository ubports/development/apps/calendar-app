/*
* Copyright (C) 2013-2014 Canonical Ltd
*
* This file is part of Lomiri Calendar App
*
* Lomiri Calendar App is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Lomiri Calendar App is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import Lomiri.Components 1.3

import "dateExt.js" as DateExt
import "colorUtils.js" as Color
import "./3rd-party/lunar.js" as Lunar

PageWithBottomEdge {
    id: monthViewPage
    objectName: "monthViewPage"

    property var anchorDate: DateExt.today();
    readonly property var firstDayOfAnchorDate: new Date(anchorDate.getFullYear(),
    anchorDate.getMonth(),
    1,
    0, 0, 0)
    readonly property var currentDate: monthViewPath.currentItem.item ?
    monthViewPath.currentItem.item.indexDate
    : null

    property var selectedDay;
    property bool displayLunarCalendar: false

    signal weekSelected(var date);
    signal dateSelected(var date);

    Keys.forwardTo: [monthViewPath]
    onAnchorDateChanged: monthViewPath.scrollToBegginer()

    Action {
        id: calendarTodayAction
        objectName:"todaybutton"
        iconName: "calendar-today"
        text: i18n.tr("Today")
        onTriggered: {
            anchorDate = new Date().midnight()
        }
    }



    header: DefaultHeader {
        id: pageHeader

        trailingActionBar {
            actions: [
                commonHeaderActions.settingsAction,
                commonHeaderActions.showCalendarAction,
                commonHeaderActions.reloadAction,
                commonHeaderActions.syncCalendarAction,
                calendarTodayAction
            ]
            numberOfSlots: 4
        }

        title: {
            if (displayLunarCalendar) {
                var year = currentDate.getFullYear()
                var month = currentDate.getMonth()
                var day = Math.floor(Date.daysInMonth(year, month) / 2.0)
                var lunarDate = Lunar.calendar.solar2lunar(year, month + 1, day)
                return i18n.tr("%1 %2").arg(lunarDate.IMonthCn).arg(lunarDate.gzYear)
            } else {
                // It's used in the header of the month and week views
                return currentDate.standaloneMonthNameCapitalised() + " " + currentDate.getFullYear()
            }
        }
        flickable: null
    }

    PathViewBase{
        id: monthViewPath
        objectName: "monthViewPath"

        anchors {
            fill: parent
            topMargin: header.height
            bottomMargin: monthViewPage.bottomEdgeHeight
        }

        delegate: Loader {
            id: delegateLoader

            width: monthViewPath.width
            height: monthViewPath.height
            active: index === monthViewPath.currentIndex

            sourceComponent: MonthWithEventsComponent {
                id: monthDelegate

                property var indexDate: firstDayOfAnchorDate.addMonths(monthViewPath.loopCurrentIndex + monthViewPath.indexType(index))

                currentMonth: indexDate.getMonth()
                currentYear: indexDate.getFullYear()
                displayLunarCalendar: monthViewPage.displayLunarCalendar

                autoUpdate: monthViewPage.active && isCurrentItem
                modelFilter: eventModel.filter
                width: parent.width - units.gu(4)
                height: parent.height
                isCurrentItem: (index === monthViewPath.currentIndex)
                isActive: !monthViewPath.moving && !monthViewPath.flicking
                displayWeekNumber: mainView.displayWeekNumber
                isYearView: false

                onWeekSelected: {
                    monthViewPage.weekSelected(date);
                }

                onDateSelected: {
                    monthViewPage.dateSelected(date);
                }

                // make sure that the model is updated after create a new event if it is marked as auto-update false
                Connections {
                    target: monthViewPage
                    onActiveChanged: {
                        if (monthViewPage.active) {
                            monthDelegate.update()
                        }
                    }
                    onEventSaved: {
                        monthDelegate.update()
                    }
                    onEventDeleted: {
                        monthDelegate.update()
                    }
                }

                Connections {
                    target: mainView
                    onSyncInProgressChanged: if (mainView.syncInProgress === false) monthDelegate.update()
                }
            }
        }
    }
}
