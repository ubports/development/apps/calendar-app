/*
  Copyright (C) 2025 UBports Foundation.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

#pragma once

#include <QDir>
#include <QString>

static const QString APP_DIR = qgetenv ("APP_DIR");
static const QString LOCALE_DIR = QStringLiteral ("@CMAKE_INSTALL_FULL_LOCALEDIR@");

inline bool isRunningInstalled (void)
{
    static bool installed = QCoreApplication::applicationDirPath() == (QDir ("@CMAKE_INSTALL_FULL_BINDIR@")).canonicalPath();
    return installed;
}

inline QString localeDirectory (void)
{
    static QString dir = QString();

    if (dir.isEmpty())
    {
        if (!APP_DIR.isEmpty())
        {
            dir = QDir::cleanPath (APP_DIR + QStringLiteral ("/") + LOCALE_DIR);
        }
        else if (isRunningInstalled())
        {
            dir = QDir::cleanPath (LOCALE_DIR);
        }
        else
        {
            dir = QDir::currentPath();
        }
    }

    return dir;
}

