cmake_minimum_required(VERSION 3.5)
project(lomiri-calendar-app VERSION 1.1.2 LANGUAGES CXX)

include(FindGettext)
if(NOT GETTEXT_FOUND)
    message(FATAL_ERROR "Could not find gettext")
endif(NOT GETTEXT_FOUND)

find_package(Qt5Core REQUIRED)
find_package(Qt5Qml REQUIRED)
find_package(Qt5Quick REQUIRED)

option(INSTALL_TESTS "Install the tests on make install" on)
option(CLICK_MODE "Installs to a contained location" on)

# Standard install paths
include(GNUInstallDirs)

# Tests
enable_testing()

# Automatically create moc files
set(CMAKE_AUTOMOC ON)

# Components PATH
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")

set(CLICK_NAME calendar.ubports)
set(DESKTOP_FILE ${PROJECT_NAME}.desktop)
set(APPLICATION_FILE "${PROJECT_NAME}.application")
set(URLS_FILE "${PROJECT_NAME}.url-dispatcher")
set(ICON_FILE lomiri-calendar-app@30.png)
set(SPLASH_FILE lomiri-calendar-app-splash.svg)
set(AUTOPILOT_DIR "lomiri_calendar_app")
set(APP_HARDCODE "lomiri-calendar-app")

if(CLICK_MODE)
    set(CMAKE_INSTALL_PREFIX /)
    set(DATA_DIR /)
    set(ICON ${ICON_FILE})
    set(SPLASH ${SPLASH_FILE})
    set(EXEC "lomiri-calendar-app-migrate.py ${CMAKE_INSTALL_BINDIR}/${APP_HARDCODE} %U")
    set(DESKTOP_DIR ${DATA_DIR})
    set(URLS_DIR ${DATA_DIR})
    set(APPLICATION_DIR ${DATA_DIR})
    configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
    install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/manifest.json
        ${CLICK_NAME}.apparmor
        ${CLICK_NAME}.contenthub
        push-helper
        push-helper.json
        calendar-helper-apparmor.json
        DESTINATION ${DATA_DIR})
else(CLICK_MODE)
    set(DATA_DIR ${CMAKE_INSTALL_DATADIR}/${APP_HARDCODE})
    set(ICON ${CMAKE_INSTALL_PREFIX}/${DATA_DIR}/${ICON_FILE})
    set(SPLASH ${CMAKE_INSTALL_PREFIX}/${DATA_DIR}/${SPLASH_FILE})
    set(EXEC ${APP_HARDCODE})
    set(DESKTOP_DIR ${CMAKE_INSTALL_DATADIR}/applications)
    set(URLS_DIR ${CMAKE_INSTALL_DATADIR}/lomiri-url-dispatcher/urls)
    set(APPLICATION_DIR ${CMAKE_INSTALL_DATADIR}/accounts/applications)
    install(FILES
            push-helper
            RENAME ${APP_HARDCODE}
            DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/lomiri-push-service/legacy-helpers)
    install(FILES calendar.ubports.contenthub
            DESTINATION ${CMAKE_INSTALL_DATADIR}/lomiri-content-hub/peers/ RENAME lomiri-calendar-app)
endif(CLICK_MODE)

# Handle i18n in the desktop file
configure_file(${DESKTOP_FILE}.in.in ${DESKTOP_FILE}.in)

add_custom_target(${DESKTOP_FILE} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE}..."
    COMMAND ${GETTEXT_MSGFMT_EXECUTABLE}
            --desktop --template=${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE}.in
            -o ${DESKTOP_FILE}
            -d ${CMAKE_SOURCE_DIR}/po
)

install(PROGRAMS lomiri-calendar-app-migrate.py DESTINATION ${DATA_DIR})
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE} DESTINATION ${DESKTOP_DIR})
install(FILES ${URLS_FILE} DESTINATION ${URLS_DIR})
install(FILES ${SPLASH_FILE} DESTINATION ${DATA_DIR})
install(FILES ${ICON_FILE} DESTINATION ${DATA_DIR})
install(FILES ${APPLICATION_FILE} DESTINATION ${APPLICATION_DIR})


add_subdirectory(src)
add_subdirectory(po)
add_subdirectory(tests)
