# Building and testing

## Requirements

You will need to have [Clickable](https://clickable-ut.dev/en/latest/) set up
to build the app.

## Viewing the app on desktop

You can run the app on desktop using the command:

    $ qmlscene qml/calendar.qml

## Building the app

Run the follwing command from inside the cloned git repository:

    $ clickable build click_build install launch

## Running the app

If you have your phone connected via USB, you can run the app on you phone using:

    $ clickable install launch

Note: you must have enabled developer mode on your phone for this to work.

## Submitting a patch upstream

If you want to submit a bug fix you can do so by forking the code to your own
repository, implementing the fixes and testing it to see if it fixed the issue.
We also ask that you run Unit tests to check if anything regressed due to the
bug fix.

If the tests fail, you will have to fix them before your bug fix can be
approved and merged. If the tests pass then commit and push your
code to your repository and that please file a
[merge request](https://gitlab.com/ubports/development/apps/lomiri-calendar-app/-/merge_requests).

## Running Tests

Please check README-Autopilot.md and README-Unittest.md on how to run the tests.
They are quite explanatory and will help you get started.

## Code Style

We are trying to use a common code style throughout the code base to maintain
uniformity and improve code clarity. Listed below are the code styles guides
that will be followed based on the language used.

* QML        - http://qt-project.org/doc/qt-5/qml-codingconventions.html
* JS, C++    - https://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
* Python     - Code should follow PEP8 and Flake regulations

Note: In the QML code convention, ignore the Javascript code section guidelines.
So the sections that should be taken into account in the QML conventions are QML
Object Declarations, Grouped Properties and Lists.

## Debugging

GDB allows one to see what is going on `inside' another program while it executes,  or what another program was doing at the moment it crashed. It is a pretty niffty tool which allows you to get the crash log that can help a developer pin point the cause of the crash.

To run GDB:

    $ gdb -ex=run --args qmlscene ../qml/calendar-app.qml

Your app is now running and monitored by GDB. Reproduce the steps in your app to make it crash. Once it does crash,

    bt

That's about it. To quit GDB, type quit to return back to the normal terminal console.

    quit
